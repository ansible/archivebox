---

- name: Install package dependencies
  ansible.builtin.apt:
    name:
      # consider replacing ripgrep with https://github.com/phiresky/ripgrep-all
      - ripgrep     # recursively searches directories for a regex pattern
      - lsb-release # Linux Standard Base version reporting utility
      - curl
      - wget
      - ffmpeg      # Tools for transcoding, streaming and playing of multimedia files
    state: present

- name: Make sure Archivebox userdata directory exists
  ansible.builtin.file:
    path: "{{ archivebox_userdata }}"
    state: directory
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"

# note, we depend on python3 role, which defines the pip_* vars
- name: Install Archivebox using pip --user
  ansible.builtin.pip:
    name:
      - archivebox # 0.6.2
      # this installs a long list of dependencies, including youtube-dl:
      # - asgiref-3.4.1
      # - asttokens-2.0.5
      # - backcall-0.2.0
      # - backports.zoneinfo-0.2.1
      # - black-21.12b0
      # - certifi-2021.10.8
      # - charset-normalizer-2.0.10
      # - click-8.0.3
      # - croniter-1.2.0
      # - dateparser-1.1.0
      # - decorator-5.1.1
      # - django-3.1.14
      # - django-extensions-3.1.5
      # - executing-0.8.2
      # - idna-3.3
      # - ipython-8.0.0
      # - jedi-0.18.1
      # - matplotlib-inline-0.1.3
      # - mypy-extensions-0.4.3
      # - parso-0.8.3
      # - pathspec-0.9.0
      # - pexpect-4.8.0
      # - pickleshare-0.7.5
      # - platformdirs-2.4.1
      # - prompt-toolkit-3.0.24
      # - ptyprocess-0.7.0
      # - pure-eval-0.2.1
      # - pygments-2.11.2
      # - python-crontab-2.6.0
      # - python-dateutil-2.8.2
      # - pytz-2021.3
      # - pytz-deprecation-shim-0.1.0.post0
      # - regex-2022.1.18
      # - requests-2.27.1
      # - setuptools-60.5.0
      # - six-1.16.0
      # - sqlparse-0.4.2
      # - stack-data-0.1.4
      # - tomli-1.2.3
      # - traitlets-5.1.1
      # - typing-extensions-4.0.1
      # - tzdata-2021.5
      # - tzlocal-4.1
      # - urllib3-1.26.8
      # - w3lib-1.22.0
      # - wcwidth-0.2.5
      # - youtube-dl-2021.12.17
    state: latest
    extra_args: "{{ pip_package_scope}} {{ pip_extra_args }}"
    executable: pip3
  become: true
  become_user: "{{ pip_become_user }}"

- name: "Create archivebox working directory {{ archivebox_pwd }}"
  ansible.builtin.file:
    path: "{{ archivebox_pwd }}"
    state: directory
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"

# note that the archivebox command will not be available in the PATH used by Ansible
- name: "Initialise Archivebox in {{ archivebox_pwd }}"
  ansible.builtin.command: >
    {{ pip_become_user_home }}/.local/bin/archivebox init
  become: true
  become_user: "{{ ansible_env.USER }}"
  args:
    chdir: "{{ archivebox_pwd }}"
    creates: "{{ archivebox_pwd }}/ArchiveBox.conf"

# https://docs.archivebox.io/en/latest/Usage.html#disk-layout
- name: "Create Archivebox's storage folders in {{ archivebox_userdata }}"
  ansible.builtin.file:
    path: "{{ archivebox_userdata }}/{{ item }}"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    state: directory
  loop:
    - archive
    - sources

# https://docs.archivebox.io/en/latest/Usage.html#disk-layout
- name: "Symlink Archivebox's storage folders to {{ archivebox_userdata }}"
  ansible.builtin.file:
    src: "{{ archivebox_userdata }}/{{ item }}"
    dest: "{{ archivebox_pwd }}/{{ item }}"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
    state: link
    force: yes
  loop:
    - archive
    - sources

# Figured out the reason: LXD container that mounts BAY must have security nesting enabled (this was missing)
# I'm getting weird warnings on the second and subsequent runs of this task (but not the first):
# "update.go:85: cannot change mount namespace according to change mount
# (/run/user/1000/doc/by-app/snap.chromium /run/user/1000/doc none bind,rw,x-snapd.ignore-missing 0 0):
# cannot inspect "/run/user/1000/doc": lstat /run/user/1000/doc: permission denied
# mkdir: cannot create directory '/run/user/1000': Permission denied
# Weird because /run/user/1000 is wholly owned by {{ ansible_env.USER }}, as expected
# Also, what's snap got to do with it?
- name: Configure Archivebox
  ansible.builtin.command: >
    {{ ansible_env.HOME }}/.local/bin/archivebox config --set {{ item }}
  loop:
    # https://docs.archivebox.io/en/latest/Configuration.html#output-dir
    - "OUTPUT_DIR={{ archivebox_pwd }}"
    # define chrome browser binary
    - "CHROME_BINARY=chromium-browser"
    # do not save to archive.org by default
    - "SAVE_ARCHIVE_DOT_ORG=False"
    # until fullpage screenshot works, increase resolution to capture most pages
    # define screenshot resolution
    - "RESOLUTION=1440,4320"
    # https://docs.archivebox.io/en/latest/Security-Overview.html#publishing
    - "FOOTER_INFO={{ email_webmaster }}"
    # https://github.com/ArchiveBox/ArchiveBox#-web-ui-usage
    - "PUBLIC_INDEX=True"
    - "PUBLIC_SNAPSHOTS=True"
    # careful, PUBLIC_ADD_VIEW=True lets anyone add new snapshots!
    - "PUBLIC_ADD_VIEW=False"
  notify: restart archivebox
  become: true
  become_user: "{{ ansible_env.USER }}"
  args:
    chdir: "{{ archivebox_pwd }}"

## Note that a superuser account is *necessary* to access the ArchiveBox web-admin pages.
## We will have to create superuser account manually until a non-interactive method is available.
## Consider editing index.sqlite3 directly, as suggested by @pirate
# https://github.com/ArchiveBox/ArchiveBox/issues/734#issuecomment-1019365563
# archivebox manage createsuperuser
# taha@taipei:/opt/archivebox
# $ archivebox manage createsuperuser
# [i] [2022-01-18 22:45:00] ArchiveBox v0.6.2: archivebox manage createsuperuser
#     > /opt/archivebox
# Username (leave blank to use 'taha'): 
# Email address: 
# Password: 
# Password (again): 
# Superuser created successfully.
# How will this task handle playbook reruns (when the superuser account has already been created)?
# It seems it just freezes, indefinitely. It seems this method will not work idempotently.
# https://github.com/ArchiveBox/ArchiveBox/issues/734
# Next version of Archivebox may offer other ways of accomplishing this (see issue 734)
# - name: Create Archivebox superuser account
#   ansible.builtin.expect:
#     command: "{{ ansible_env.HOME }}/.local/bin/archivebox manage createsuperuser"
#     # command: /bin/bash -c "cd {{ archivebox_pwd }} && {{ ansible_env.HOME }}/.local/bin/archivebox manage createsuperuser"
#     timeout: 10
#     responses:
#       'Username': "{{ archivebox_admin.user }}"
#       'Email address': "{{ archivebox_admin.email }}"
#       'Password': "{{ archivebox_admin.pwd }}"
#       'Password (again)': "{{ archivebox_admin.pwd }}"
#   become: true
#   become_user: "{{ ansible_env.USER }}"
#   args:
#     chdir: "{{ archivebox_pwd }}"
#   # no_log: true

- name: Create systemd service
  ansible.builtin.template:
    src: archivebox.service.j2
    dest: /etc/systemd/system/archivebox.service
    owner: root
    group: root
    mode: u=rw,go=r

- name: Start the Archivebox systemd service
  ansible.builtin.systemd:
    daemon_reload: yes
    enabled: yes
    state: started
    name: archivebox

- ansible.builtin.import_tasks: backup.yml
  tags: backups-only
