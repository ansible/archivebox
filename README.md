# Archivebox

This Ansible role installs [ArchiveBox](https://archivebox.io) on Ubuntu 20.04,
specifically on Ubuntu Focal running in an LXC container.

Excellent [step-by-step installation guide](https://sleeplessbeastie.eu/2021/07/12/how-to-install-archivebox-on-raspberrypi/), which this role is primarily based on.

After running this role, you will need to create an admin user manually:
```
taha@taipei:/opt/archivebox
$ archivebox manage createsuperuser
```

## Known issues

It's and up and running, but there are some irregularities.

+ Adding a URL still saves to archive.org by default.
+ I created a "regular" account, but could not use it to add links, even after giving it
  all reasonable permissions *and* staff account permission. Weird. Just use the superuser
  account for now.
+ Fullpage screenshots is waiting on some developments ([#80](https://github.com/ArchiveBox/ArchiveBox/issues/80), [#51](https://github.com/ArchiveBox/ArchiveBox/issues/51)).

Chromium browser needs to be installed without relying on snap, otherwise we
get error-looking warnings about
```
archivebox[7063]: mkdir: cannot create directory '/run/user/1000': Permission denied
```
This was caused by a known issue - Chromium is installed using snap by default,
and snap and LXC containers don't work well together, apparently.

The solution was to install Chromium wihout snap (this is taken care of in my
`chromium-browser` role).

+ https://github.com/lxc/lxd/issues/5844
+ https://discuss.linuxcontainers.org/t/installing-chromium-in-container-fails-sudo-snap-install-chromium/9104/3


## Refs

+ https://github.com/ArchiveBox/ArchiveBox/wiki/Install
+ https://docs.archivebox.io/en/latest/Install.html#ubuntu-debian
+ https://github.com/ArchiveBox/ArchiveBox/wiki/Publishing-Your-Archive
+ https://news.ycombinator.com/item?id=26858635
+ [example Archivebox configuration files](https://github.com/ArchiveBox/ArchiveBox/tree/457556a2a8de5bb6f1a84c3712d714c531fdfa31/etc)

### Other Ansible playbooks for Archivebox

+ https://github.com/Pro-Tweaker/SEEDbox/blob/main/roles/archivebox/tasks/main.yml (using Docker)


### Other self-hostable web archiving projects

+ https://www.linkace.org
+ https://archivy.github.io
+ https://github.com/WorldBrain/Memex
+ https://awesomeopensource.com/projects/web-archiving
+ https://getpolarized.io (not FOSS nor self-hostable)
+ https://github.com/Atarity/deploy-your-own-saas
